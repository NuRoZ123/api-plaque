<?php

    include "bdd.php";

    if(isset($_GET["get"]))
    {
        $plaque = getPlaque($_GET["get"]);

        if(sizeof($plaque) > 0)
        {
            echo '{"' . $_GET["get"] . '":{"nbrReport": ' . $plaque[0]["nbrRepport"] . ',"lastReport": "' . $plaque[0]["lastReport"] . '"}}';
        }
        else
        {
            echo '{"' . $_GET["get"] . '":{"nbrReport": 0,"lastReport": "0000-00-00 00:00:00"}}';
        }
    }

    if(isset($_GET["add"]))
    {
        $date = $date = new dateTime();

        $plaque = getPlaque($_GET["add"]);
        if(sizeof($plaque) == 0)
        {
            $date = $date->format("Y-m-d H:i:s");
            creePlaque($_GET["add"], 1, $date);
        }
        else
        {
            $dateEnd = new DateTime($plaque[0]["lastReport"]);
            $dateDiff = $date->diff($dateEnd);

            if($dateDiff->y > 0 || $dateDiff->days > 0 || $dateDiff->m > 0 || $dateDiff->h > 0 || $dateDiff->i >= 10)
            {
              $newNbrReport = ((int) $plaque[0]["nbrRepport"]) + 1;
              updatePlaque($_GET["add"], $newNbrReport);
            }
        }
    }

?>