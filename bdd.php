<?php

function connectSQL()
{
    $ini = parse_ini_file("bdd.ini", true);
    return new PDO("mysql:host=" . $ini["connexion"]["domaine"] . ";dbname=" . $ini["connexion"]["dbName"], $ini["connexion"]["id"], $ini["connexion"]["pass"]);
}

function getPlaque(string $plaque)
{
    $bdd = connectSQL();
    $req = $bdd->query("SELECT * FROM plaque where plaque='" . $plaque . "'");
    return $req->fetchAll();
}

function creePlaque(string $plaque, int $nbrReport, string $dateTime)
{
    $bdd = connectSQL();
    $req = $bdd->prepare("INSERT INTO plaque(plaque, nbrRepport, lastReport) VALUE(?,?,?)");
    $req->execute(array($plaque,$nbrReport, $dateTime));
}

function updatePlaque(string  $plaque, int $nbr)
{
    $date = new dateTime();
    $date = $date->format("Y-m-d H:i:s");

    $bdd = connectSQL();
    $req = $bdd->prepare("UPDATE plaque SET nbrRepport=?, lastReport=? WHERE plaque=?");
    echo $req->execute(array($nbr, $date, $plaque)) ? "t":"f";
}

?>
